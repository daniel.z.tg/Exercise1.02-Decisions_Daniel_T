package me.danielzgtg.compsci11_sem2_2017.decisions;

import static me.danielzgtg.compsci11_sem2_2017.decisions.PartThree.hasValidCombination;

/**
 * Test case for Part3
 * 
 * @author Daniel Tang
 * @since 14 February 2017
 */
public final class PartThreeTestCase {

	public static void main(final String[] ignore) {
		if (!hasValidCombination(new Integer[] { 1, 2, 3 }) || // pass
				!hasValidCombination(new Integer[] { 6, 3, 18 }) || // pass
				!hasValidCombination(new Integer[] { 4, 9, 5 }) || // pass
				hasValidCombination(new Integer[] { 3, 8, 4 }) || // fail
				!hasValidCombination(new Integer[] { 5, 10, 2 }) || // pass
				hasValidCombination(new Integer[] { 13, 4, 3 }) || // fail
				hasValidCombination(new Integer[] { 3, 0, 4 }) || // fail
				hasValidCombination(new Integer[] { 0, 3, 4 }) || // fail
				!hasValidCombination(new Integer[] { 0, 1, 0 }) // pass
				) {
			throw new AssertionError("Tests did not pass!");
		}

		System.out.println("Tests passed!");
	}

	private PartThreeTestCase() { throw new UnsupportedOperationException(); }
}
