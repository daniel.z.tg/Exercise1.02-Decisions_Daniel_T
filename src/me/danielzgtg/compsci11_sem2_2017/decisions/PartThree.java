package me.danielzgtg.compsci11_sem2_2017.decisions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;

/**
 * A number relations game.
 * 
 * @author Daniel Tang
 * @since 10 Feb 2017
 */
@SuppressWarnings("OptionalGetWithoutIsPresent")
public final class PartThree {

	/**
	 * The greeting message.
	 */
	private static final String INTRO_MSG =
			"Welcome to my fancy new game!";

	/**
	 * Prompt layout for first integer.
	 */
	private static final String NUMBER_PROMPT1 =
			"What is the first integer? : ";

	/**
	 * Prompt layout for second integer.
	 */
	private static final String NUMBER_PROMPT2 =
			"What is the second integer? : ";

	/**
	 * Prompt layout for third integer.
	 */
	private static final String NUMBER_PROMPT3 =
			"What is the third integer? : ";

	/**
	 * The message after winning (valid combination found).
	 */
	private static final String OUTPUT_WINNING =
			"CONGRATULATIONS - YOU WON!!!";

	/**
	 * The message after losing (no valid combination found).
	 */
	private static final String OUTPUT_LOSING =
			"Sorry, better luck next time.";

	/**
	 * The list of operations to combining,
	 * where the integers are a, b, c in order,
	 * and the operations check a ? b == c
	 * where ? is a basic arithmetic operation.
	 */
	private static final List<Predicate<Integer[]>> OPERATIONS;

	static {
		// Add operations to the list (workaround for java generic arrays)
		List<Predicate<Integer[]>> tempList = new ArrayList<>();

		// a + b == c and b + a == c
		tempList.add((Integer[] x) -> x[0] + x[1] == x[2]);
		// a - b == c
		tempList.add((Integer[] x) -> x[0] - x[1] == x[2]);
		// b - a == c
		tempList.add((Integer[] x) -> x[1] - x[0] == x[2]);
		// a * b == c and b * a == c
		tempList.add((Integer[] x) -> x[0] * x[1] == x[2]);
		// a / b == c, equivalent to b * c == a when b != 0
		tempList.add((Integer[] x) -> x[1] != 0 && x[1] * x[2] == x[0]);
		// b / a == c, equivalent to a * c == b when a != 0
		tempList.add((Integer[] x) -> x[0] != 0 && x[0] * x[2] == x[1]);

		// Store the list
		OPERATIONS = Collections.unmodifiableList(tempList);
	}

	public static void main(final String[] ignore) {
		final Integer[] nums = new Integer[3];

		// Greetings!
		System.out.println(INTRO_MSG);

		try (final Scanner scanner = new Scanner(System.in)){
			// Obtain 3 integers
			nums[0] = Tools.promptInt(scanner, NUMBER_PROMPT1);
			nums[1] = Tools.promptInt(scanner, NUMBER_PROMPT2);
			nums[2] = Tools.promptInt(scanner, NUMBER_PROMPT3);
		}

		// Check and print out the result
		System.out.println(
				hasValidCombination(nums) ?	OUTPUT_WINNING : OUTPUT_LOSING);
	}

	/**
	 * Determines whether there is a valid combination of operators that would
	 * fits the input numbers.
	 * 
	 * @param nums - An {@code Integer} array containing the variables a, b, and c
	 * @see OPERATIONS
	 * @return {@code true} if there is a valid combination.
	 */
	public static final boolean hasValidCombination(final /*immutable*/ Integer[] nums){
		// First get the possible statements in a stream
		return OPERATIONS.stream()
				// Then check the stream against all of them into a boolean stream
				.map(x -> x.test(nums))
				// Check if there is at least one valid combination in the stream
				.reduce((a, b) -> a || b).get();
	}

	private PartThree() { throw new UnsupportedOperationException(); }
}
