package me.danielzgtg.compsci11_sem2_2017.decisions;

import java.util.Scanner;

/**
 * A calculator for buying DVDs.
 * 
 * @author Daniel Tang
 * @since 10 Feb 2017
 */
public final class PartFour {

	/**
	 * Prompt layout for price of DVDs.
	 */
	private static final String PRICE_PROMPT =
			"How much does the DVD movie cost? : $";

	/**
	 * Error message for negative price.
	 */
	private static final String PRICE_ERROR_NEGATIVE =
			"A price cannot be negative!";

	/**
	 * Prompt layout for quantity of DVDs.
	 */
	private static final String QUANTITY_PROMPT =
			"How many DVD's would you like to purchase? : x";

	/**
	 * Error message for negative quantity.
	 */
	private static final String QUANTITY_ERROR_NEGATIVE =
			"A quantity cannot be negative!";

	/**
	 * Rate of tax, subtotal times tax added to itself is total.
	 */
	private static final double TAX_RATE = 0.13D;

	/**
	 * The output layout for the calculated totals.
	 */
	private static final String OUTPUT_LAYOUT =
			"Your total is $%.2f and your subtotal was $%.2f.";

	public static void main(final String[] ignore) {
		final double price;
		final int quantity;

		try (final Scanner scanner = new Scanner(System.in)) {
			// Prompt for price as decimal
			price = Tools.promptDouble(scanner, PRICE_PROMPT);

			if (price < 0) {
				// Complain and error-exit
				System.err.println(PRICE_ERROR_NEGATIVE);
				System.exit(1);
			}

			// Prompt for price as integer
			quantity = Tools.promptInt(scanner, QUANTITY_PROMPT);

			if (quantity < 0) {
				// Complain and error-exit
				System.err.println(QUANTITY_ERROR_NEGATIVE);
				System.exit(1);
			}
		}

		// Calculate subtotal and taxed total
		final double total, subTotal;
		subTotal = price * quantity;
		total = subTotal * (1 + TAX_RATE);

		// Print out the result
		System.out.format(OUTPUT_LAYOUT, total, subTotal);
	}

	private PartFour() { throw new UnsupportedOperationException(); }
}
