package me.danielzgtg.compsci11_sem2_2017.decisions;

import java.util.Scanner;

/**
 * Tools for the other parts to use.
 * 
 * @author Daniel Tang
 * @since 10 Feb 2017
 */
@SuppressWarnings({"InfiniteLoopStatement", "StatementWithEmptyBody"})
/*packaged*/ final class Tools {

	/**
	 * The error message when the user did not enter a valid integer.
	 */
	private static final String NUMBER_INT_ERROR_MSG =
			"That is not a valid integer!";

	/**
	 * The error message when the user did not enter a valid decimal.
	 */
	private static final String NUMBER_DOUBLE_ERROR_MSG =
			"That is not a valid decimal!";

	/**
	 * Obtains an {@code int} from the user.
	 * 
	 * @param scanner The scanner to interact with the user.
	 * @param prompt The message to prompt the user with.
	 * @return  The obtained {@code int}
	 */
	/*packaged*/ static final int promptInt(final Scanner scanner, final String prompt) {
		// Print the prompt, and wait for the integer input to return
		System.out.print(prompt);

		if (scanner.hasNextInt()) {
			return scanner.nextInt();
		} else {
			// If not integer, complain and error-exit
			System.err.println(NUMBER_INT_ERROR_MSG);
			System.exit(1);
			while (true) {
				// Not reachable, but required for compile
			}
		}
	}
	/**
	 * Obtains an {@code double} from the user.
	 * 
	 * @param scanner The scanner to interact with the user.
	 * @param prompt The message to prompt the user with.
	 * @return  The obtained {@code double}
	 */
	/*packaged*/ static final double promptDouble(final Scanner scanner, final String prompt) {
		// Print the prompt, and wait for the double input to return
		System.out.print(prompt);

		if (scanner.hasNextDouble()) {
			return scanner.nextDouble();
		} else {
			// If not double, complain and error-exit
			System.err.println(NUMBER_DOUBLE_ERROR_MSG);
			System.exit(1);
			while (true) {
				// Not reachable, but required for compile
			}
		}
	}

	private Tools() { throw new UnsupportedOperationException(); }
}
