package me.danielzgtg.compsci11_sem2_2017.decisions;

import java.util.Scanner;

/**
 * A lottery ticket checker.
 * 
 * @author Daniel Tang
 * @since 10 Feb 2017
 */
public final class PartOne {

	/**
	 * The secret winning lottery ticket number
	 */
	private static final int NUMBER_WINNING = 34567;

	/**
	 * Prompt layout for lottery ticket number input.
	 */
	private static final String NUMBER_PROMPT =
			"What is your five-digit lottery ticket number? : ";

	/**
	 * Error message for a lottery ticket that is not five digits.
	 */
	private static final String NUMBER_ERROR_MSG_NOT_FIVE =
			"That is an impossible lottery number!";

	/**
	 * The message after losing (did not match winning number).
	 */
	private static final String OUTPUT_LOSING = "Better luck next time!";

	/**
	 * The message after winning (matched winning number).
	 */
	private static final String OUTPUT_WINNING = "You have won $1,000,000";

	public static void main(final String[] ignore) {
		try (final Scanner scanner = new Scanner(System.in)){
			// Prompt for lottery ticket number
			final int userNumber = Tools.promptInt(scanner, NUMBER_PROMPT);

			// Make sure it is five digits
			if ((userNumber < 10000) || (userNumber > 99999)) {
				System.err.println(NUMBER_ERROR_MSG_NOT_FIVE);
				System.exit(1);
			}

			// Print out whether they won
			System.out.println(userNumber == NUMBER_WINNING ?
					OUTPUT_WINNING : OUTPUT_LOSING);
		}
	}

	private PartOne() { throw new UnsupportedOperationException(); }
}
