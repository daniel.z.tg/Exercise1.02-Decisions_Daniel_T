package me.danielzgtg.compsci11_sem2_2017.decisions;

import java.util.Scanner;

/**
 * A program to compare numbers.
 * 
 * @author Daniel Tang
 * @since 10 Feb 2017
 */
public final class PartTwo {

	/**
	 * Prompt layout for first integer.
	 */
	private static final String NUMBER_PROMPT1 =
			"What is the first integer? : ";

	/**
	 * Prompt layout for second integer.
	 */
	private static final String NUMBER_PROMPT2 =
			"What is the second integer? : ";

	/**
	 * The output layout when the numbers are the same.
	 */
	private static final String NUMBERS_SAME_OUTPUT_LAYOUT =
			"The numbers are both %d.";

	/**
	 * The output layout when one number is greater.
	 */
	private static final String NUMBERS_REGULAR_OUTPUT_LAYOUT =
			"%d is larger than %d.";

	public static void main(final String[] ignore) {
		final int num1, num2;

		try (final Scanner scanner = new Scanner(System.in)) {
			// Prompt for two numbers
			num1 = Tools.promptInt(scanner, NUMBER_PROMPT1);
			num2 = Tools.promptInt(scanner, NUMBER_PROMPT2);
		}

		if (num1 == num2) {
			// When the numbers are the same, display the message
			System.out.format(NUMBERS_SAME_OUTPUT_LAYOUT, num1);
		} else if (num1 > num2) {
			// When n1 is greater than n2, display n1 in front (being bigger)
			System.out.format(NUMBERS_REGULAR_OUTPUT_LAYOUT, num1, num2);
		} else /*if (num2 > num1)*/ {
			// When n2 is greater than n1, display n2 in front (being bigger)
			System.out.format(NUMBERS_REGULAR_OUTPUT_LAYOUT, num2, num1);
		}
	}

	private PartTwo() { throw new UnsupportedOperationException(); }
}
